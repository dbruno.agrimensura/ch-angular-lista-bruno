import { Component } from '@angular/core';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrl: './data-list.component.scss'
})
export class DataListComponent {
  personas: any[] = [
  { nombre: 'Hidan', aldea: 'Aldea de las Aguas Termales' },
  { nombre: 'Sasori', aldea: 'Aldea Oculta de la Arena' },
  { nombre: 'Kakuzu', aldea: 'Aldea Oculta de la Cascada' },
  { nombre: 'Deidara', aldea: 'Aldea Oculta entre las Rocas' },
  { nombre: 'Kisame', aldea: 'Aldea Oculta de la Niebla' },
  { nombre: 'Itachi', aldea: 'Aldea Oculta de la Hoja' },
  { nombre: 'Zetsu', aldea: 'Se desconoce' },
  { nombre: 'Obito', aldea: 'Aldea Oculta de la Hoja' }
];

filaClicada: number = -1;

  constructor() { }

  ngOnInit(): void {
  }

  obtenerClaseAldea(aldea: string): string {
    switch (aldea) {
      case 'Aldea de las Aguas Termales':
        return 'aldea1';
      case 'Aldea Oculta de la Arena':
        return 'aldea2';
      case 'Aldea Oculta de la Hoja':
        return 'aldea3';
      case 'Aldea Oculta de la Niebla':
        return 'aldea4';
      case 'Aldea Oculta de la Cascada':
        return 'aldea5';
      case 'Aldea Oculta entre las Rocas':
        return 'aldea6';
      default:
        return '';
    }
  }
}
